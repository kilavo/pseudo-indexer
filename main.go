package kilavo_demo_indexer


/*
 * Kilavo Indexer pseudo code
 * Just to show what an indexer would look like
 */

func main(){
	/*
	 * This programm is only run when needed, Kilavo registers this indexer and
	 * what Events it listen to, whenever a relevant event occurs this is spanned
	 * as a short-lived function, processes the event, and exits
	 */

	/*
	 * will conenct to Kilavo and also get information about the Context of this
	 * Execution, such as Block or operation associated with the Event
	 */
	k := KilavoIndexerSDK()
	k.On("storage_change_KT1AbYeDbjjcAnV1QK7EZUUdqku77CdkTuv6").Exec(OnContractCall)
	k.Run()
}

func OnContractCall(update StorageUpdate, k *KilavoSDK) {
	if update.Before.tokenPool != update.After.tokenPool {
		/*
		 * Indexers only need to save Data into one of the provided
		 * Data Models, Kilavo will make them availaible to clients
		 * without the need to code your own API etc
		 */
		k.DB.TimeSeries.Set("token_pool",update.After.tokenPool)

		/*
		 * K can also manage off-chain Events that get sent
		 * published via the libp2p PubSub protocol
		 */
		k.Events.Send("update_token_pool",update.After.tokenPool)
	}

	...

}
