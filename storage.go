package kilavo_demo_indexer

type accounts struct {
	Key string
	Value AccountsValue
}

type AccountsValue struct {
	balance int
	tokens map[string]int
}

type Storage struct {
	Accounts []accounts
	SelfIsUpdating bool
	FreezeBaker bool
	LqTotal int
	manager string
	tokenPool int
	XtzPool int
}

type StorageUpdate struct {
	Before *Storage
	After *Storage
}